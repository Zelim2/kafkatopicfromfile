addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "1.0.0")
ThisBuild / libraryDependencySchemes += "org.scala-lang.modules" %% "scala-xml" % VersionScheme.Always
addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.12")
