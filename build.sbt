ThisBuild / version := "0.1"

ThisBuild / scalaVersion := "2.13.8"

lazy val typesafeVersion = "1.4.2"
lazy val akkaVersion = "2.5.23"
lazy val akkaHttpVersion = "10.2.0"
lazy val log4jVersion = "2.9.1"


ThisBuild / evictionErrorLevel := Level.Info

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % typesafeVersion ,
  "org.apache.logging.log4j" % "log4j-api" % log4jVersion,
  "log4j" % "log4j" % "1.2.17" % "compile",
  "org.scalatest" %% "scalatest-funsuite" % "3.3.0-SNAP2" % Test,
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
  "org.apache.kafka" %% "kafka" % "2.6.3",
  "org.apache.kafka" % "kafka-clients" % "2.3.0",
)

lazy val root = (project in file("."))
  .settings(
    name := "kafkatopicfromfile",
    assemblyJarName := "job.jar"
 )

assembly / test := {}
Test / testOptions += Tests.Setup(() => System.setSecurityManager(null))
