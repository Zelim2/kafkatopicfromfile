import Parameters.Config
import org.scalatest.funsuite.AnyFunSuite
import producer.readingFile

class Test extends AnyFunSuite with Config {
  val testRead: readingFile = new readingFile(PATH, TOPIC)

  test("test getFilePath"){
      assert(testRead.getFilePath.toFile.isFile)
  }
}
