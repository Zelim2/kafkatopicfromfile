import Parameters.Config
import producer.readingFile

object Main extends Config {

  val stream = new readingFile(PATH, TOPIC)

  def main(args: Array[String]): Unit = {
    stream.getSource
      .via(stream.getFlow)
      .runWith(stream.getSink)
      .andThen {
        case _ => actorSystem.terminate()
      }
  }

}
