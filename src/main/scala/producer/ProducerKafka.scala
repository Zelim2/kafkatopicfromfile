package producer


import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.log4j.Logger

import java.util.Properties

class ProducerKafka(address: String, port: String)(implicit log: Logger) {

  val props: Properties = new Properties()
  props.put("bootstrap.servers", s"$address:$port")
  props.put("key.serializer",
    classOf[StringSerializer].getName)
  props.put("value.serializer",
    classOf[StringSerializer].getName)
  props.put("acks", "all")


  def writeRecord(topic: String, key: String = null, value: String): Unit = {
    log.warn(s"записываем строку $value в топик $topic")
    new KafkaProducer[String, String](props).send(new ProducerRecord[String, String](topic, key, value)).get()
    log.warn("Ok")
  }
}
