package producer

import akka.Done
import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import akka.stream.{ActorMaterializer, IOResult}
import akka.stream.scaladsl.{FileIO, Framing, Sink, Source}
import akka.util.ByteString

import java.nio.file.{Path, Paths}
import scala.concurrent.Future

class readingFile(path: String, topic: String)
                 (implicit producer: ProducerKafka, actorSystem: ActorSystem, materializer: ActorMaterializer) {

  def getFilePath: Path = Paths.get(path)

  def getSource: Source[ByteString, Future[IOResult]] = FileIO.fromPath(getFilePath)

  def getFlow = Framing.delimiter(ByteString(System.lineSeparator()), maximumFrameLength = 512, allowTruncation = true)
    .map(_.utf8String)

  def getSink: Sink[String, Future[Done]] = Sink.foreach[String](x => producer.writeRecord(topic , value = x))

}