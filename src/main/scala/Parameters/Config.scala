package Parameters

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.apache.log4j.Logger
import producer.ProducerKafka

trait Config {

  val PATH = "src/main/resources/data.csv"
  val TOPIC = "topic2"
  val ADRESS = "localhost"
  val PORT = "9092"

  implicit val log: Logger = Logger.getLogger("logger")
  implicit val producer = new ProducerKafka(ADRESS, PORT)

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executioncontext = materializer.executionContext

}
