package consumer

import Parameters.Config

import java.util.{Collections, Properties}
import org.apache.kafka.clients.consumer.{ConsumerRecord, KafkaConsumer}
import org.apache.kafka.common.serialization.StringDeserializer

import scala.collection._
import scala.jdk.CollectionConverters.{IterableHasAsScala, SeqHasAsJava}

object Consumer extends Config{

  val properties = new Properties()
  properties.put("bootstrap.servers", s"$ADRESS:$PORT")
  properties.put("group.id", "consumer")
  properties.put("key.deserializer", classOf[StringDeserializer])
  properties.put("value.deserializer", classOf[StringDeserializer])

  val kafkaConsumer = new KafkaConsumer[String, String](properties)
  kafkaConsumer.subscribe(Seq(TOPIC).asJava)

  def printResults(): Unit = {
    val results: Iterable[ConsumerRecord[String, String]] = kafkaConsumer.poll(2000).asScala
    results.foreach(println)
  }

  def main(args: Array[String]): Unit = {
    printResults()
  }
}
